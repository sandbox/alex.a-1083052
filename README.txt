Defines a block that displays a different message every day.

The message is created from a node which is picked from a source of nodes.
The pool of nodes that are rotated into the message of the day are configurable and extendable.
The module comes with submodules that implement message pools as:
 + nodes flagged with a flag using the flag module.
 + nodes that have a certain content type.

To use it with the flag module:
 * Enable the Message of the Day block.
 * Give yourself the permission 'administer message of the day'.
 * Define a flag type of content type node (only nodes are supported currently).
 * Configure the message of the day block.  Select the flag type to use. 
 * Flag a few nodes that you want to use as messages of the day.  See the flag module for instructions.
 => The block now displays a node from the flagged nodes.  Each day a different node will be displayed.

The pool of messages is displayed by the /motd page.

Copyright 2011 Alexandros Athanasopoulos