<?php

define( MOTD_PERMISSION, 'administer message of the day');

/**
 * Implementation of hook_perm
 */
function motd_perm() {
  return array('administer message of the day');
}

/**
 * Implementation interface for providing messages of the day.
 */
abstract class motd_Provider {
  /**
   * The lists available.  An array of list_id => list_name
   * @return unknown_type
   */
  abstract function lists();
  /**
   * Load the current message.
   * @param unknown_type $list_id The identifier of the list to use.
   * @param unknown_type $mid $The message identifier.
   * @return unknown_type An object with fields mid, title, nid.
   */
  abstract function loadMessage($list_id, $mid);
/**
 * Load all messages in a list.
 * @return unknown_type A list of objects with fields mid, title, nid.
 */
  abstract function listMessages($list_id);
}

function motd_provider() {
  return new motd_composite();
}
/**
 * Implementation of hook_menu
 */
function motd_menu() {
  $items = array();
  $items['motd'] = array(
    'title' => 'Message of the Day List',
    'page callback' => 'motd_list',
    'page arguments' => array(),
    'access arguments' => array(MOTD_PERMISSION),
    'description' => 'Display the messages in the message of the day list.',
    'type' => MENU_CALLBACK,
  );
  $items['motd/select/%'] = array(
    'title' => 'Use',
    'page callback' => 'motd_select',
    'page arguments' => array(2),
    'access arguments' => array(MOTD_PERMISSION),
    'description' => 'Advance the message of the day to the next message',
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Select the item to display in the message of the day block.
 * @param unknown_type $fcid
 * @return unknown_type
 */
function motd_select($mid) {
  variable_set('motd_mid', $mid );
  drupal_goto('motd');
}

/**
 * Block Definition
 * @param unknown_type $op
 * @param unknown_type $delta
 * @param unknown_type $edit
 * @return unknown_type
 */
function motd_block( $op = 'list', $delta = 0, $edit = array() ) {
  if ( $op == 'list' ) {
    $blocks = array();
    $blocks[] = array(
        'info' => t('Message of the Day'),
        'cache' => BLOCK_CACHE_GLOBAL,
    );
    return $blocks;
  }
  elseif ( $op == 'configure' && $delta == 0 ) {
    $provider = motd_provider();
    $form = array();
    $form['motd_list_id'] = array(
        '#type' => 'select',
        '#options' => $provider->lists(),
        '#default_value' => motd_list_id(),
        '#title' => t('Node Source'),
        '#description' => t('The pool of nodes to use for the messages.'),
    );
    $form['motd_block'] = array(
        '#type' => 'textarea',
        '#title' => t('Block Template'),
        '#description' => t('The block contents (HTML).  ${nid} is replaced by the node id.  ${title} is replaced by the node title.'),
        '#default_value' => variable_get('motd_block', FALSE ),
    );
    return $form;
  }
  elseif ($op == 'save' && $delta == 0) {
    variable_set('motd_block', $edit['motd_block'] );
    variable_set('motd_list_id', $edit['motd_list_id'] );
  }
  elseif ( $op == 'view' && $delta == 0 ) {
    return array('subject' => t('Message of the Day'),
          'content' => motd_show_block());
  }
}

//////////////////////////////////////////////////////////
/**
 * The list id to use.
 * @return unknown_type
 */
function motd_list_id() {
  return variable_get('motd_list_id', NULL);
}

/**
 * List the messages of the day.
 * @return unknown_type
 */
function motd_list() {
  $mid = variable_get('motd_mid', 0 );
  $rows = array();
  foreach( motd_provider()->listMessages(motd_list_id()) as $r ) {
    $title = $r->title;
    $selected = ($mid == $r->mid);
    if ( $selected ) {
      $title = '* ' . $title;
    }
    $rows[] = array(l($title, "/node/$r->nid"), $selected ? NULL :l(t('Use'), "motd/select/$r->mid"));
  }
  return theme_table( NULL, $rows );
}

/**
 * Generate the block contents.
 * @param $nid The node id of the node to display in the block.
 * @param $title The node title to display in the block.
 * @return unknown_type
 */
function motd_create_block($nid, $title) {
  $block = variable_get('motd_block', NULL );
  $block = trim($block);
  if ( ! $block ) {
    $block = '<a href="node/${nid}">${title}</a>';
  }
  $block = preg_replace( '/\$\{nid\}/', $nid, $block);
  $block = preg_replace( '/\$\{title\}/', $title, $block);
  return $block;
}

/**
 * Generate the block contents.
 * @return unknown_type
 */
function motd_show_block() {
  $mid = variable_get('motd_mid', 0 );
  $last_time = variable_get('motd_time', 0 );
  $time = 0 + date( 'Ymd', time() );
  if ( $last_time != $time ) {
    $mid = $mid + 1;
    variable_set('motd_time', $time );
    variable_set('motd_mid', $mid );
  }
  $provider = motd_provider();
  $option_id = motd_list_id();
  $m = $provider->loadMessage($option_id, $mid);
  if ( ! $m && $mid != 0 ) {
    $mid = 0;
    $m = $provider->loadMessage($option_id, $mid);
  }
  if ( $m ) {
    if ( $mid != $m->mid )
    variable_set('motd_mid', $m->mid );
    return motd_create_block($m->nid, $m->title);
  }
  return NULL;
}

class motd_composite extends motd_Provider {
  static $providers;

  function get_providers() {
    if ( ! $providers ) {
      $providers = module_invoke_all('motd_provider');
    }
    return $providers;
  }
  function lists() {
    $options = array();
    foreach( $this->get_providers() as $provider_type => $provider ) {
      foreach( $provider->lists() as $list_id => $list_title ) {
        $options["$provider_type.$list_id"] = "$provider_type: $list_title"; 
      }
    }
    return $options;
  }

  function loadMessage($list_id, $mid) {
    $fields = explode( '.', $list_id, 2 );
    $providers = $this->get_providers();
    $provider = $providers[$fields[0]];
    return $provider ? $provider->loadMessage($fields[1], $mid) : NULL;
  }
  
  function listMessages($list_id) {
    $fields = explode( '.', $list_id, 2 );
    $providers = $this->get_providers();
    $provider = $providers[$fields[0]];
    return $provider ? $provider->listMessages($fields[1]) : array();
  }
}
